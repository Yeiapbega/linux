<?php

// Credenciales de prueba
$user = "xxxx";
$pass = "xxxx";

// Datos de acceso al servidor LDAP
$host = "localhost";
$port = "389";

// Conexto donde se encuentran los usuarios
$basedn = "ou=people,dc=linuxito,dc=com";

// Atributos a recuperar
$searchAttr = array("dn", "cn", "sn", "givenName");

// Atributo para incorporar en la respuesta
$displayAttr = "cn";

// Respuesta por defecto
$status = 1;
$msg = "";
$userDisplayName = "null";

// Recuperar datos del POST
if (isset($_POST['user'])) {
        $user = $_POST['user'];
}
if (isset($_POST['pass'])) {
        $pass = $_POST['pass'];
}

// Establecer la conexi�n con el servidor LDAP
$ad = ldap_connect("ldap://{$host}:{$port}") or die("No se pudo conectar al servidor LDAP.");

// Autenticar contra el servidor LDAP
ldap_set_option($ad, LDAP_OPT_PROTOCOL_VERSION, 3);
if (@ldap_bind($ad, "uid={$user},{$basedn}", $pass)) {
        // En caso de �xito, recuperar los datos del usuario
        $result = ldap_search($ad, $basedn, "(uid={$user})", $searchAttr);
        $entries = ldap_get_entries($ad, $result);
        if ($entries["count"]>0) {
                // Si hay resultados en la b�squeda
                $status = 0;
                if (isset($entries[0][$displayAttr])) {
                        // Recuperar el atributo a incorporar en la respuesta
                        $userDisplayName = $entries[0][$displayAttr][0];
                        $msg = "Autenticado como {$userDisplayName}";
                }
                else {
                        // Si el atributo no est� definido para el usuario
                        $userDisplayName = "-";
                        $msg = "Atributo no disponible ({$displayAttr})";
                }
        }
        else {
                // Si no hay resultados en la b�squeda, retornar error
                $msg = "Error desconocido";
        }
}
else {
        // Si falla la autenticaci�n, retornar error
        $msg = "Usuario y/o contrase�a inv�lidos";
}

// Respuesta en formato JSON
header('Content-Type: application/json');
echo "{\"uid\": \"{$user}\", \"estado\": \"{$status}\", \"nombre\": \"{$userDisplayName}\", \"debug\": \"{$msg}\"}";
?>